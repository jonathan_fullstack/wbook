BEGIN

CREATE TABLE IF NOT EXISTS `t_author` (
  `author_id` bigint NOT NULL,
  `date_to_create` date DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `social_status` int DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`author_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

INSERT INTO `t_author` (`author_id`, `date_to_create`, `firstname`, `lastname`, `social_status`, `username`, `password`, `provider`, `email`) VALUES
(1, NULL, 'nathan', 'nathan', NULL, 'nathan', '$2a$10$rRqRYmAtcFOS5Xkpj1EPsu60sxlKnZqydzU8RqcafNmHZ6vdZrLNW', NULL, 'nathan@nathan.fr'),
(2, NULL, 'test1', 'test1', NULL, 'test1test1', '$2a$10$3/NQzO.SduIbeRX/ydOF1eTfbYU/vXm4.CP3QdaLia2u5bj1kHNp.', NULL, 'test1@test1.fr');

END

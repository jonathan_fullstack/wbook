package wbook.wbook.model;

public enum CategoryBook {
  CHILD,
  ROMANCE,
  THRILLER,
  POLITIC,
  EROTIC,
  NOUVELLE,
  POESIE,
  INCONNU;

}

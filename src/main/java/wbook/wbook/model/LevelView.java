package wbook.wbook.model;

public enum LevelView {
  CHILD,
  TEEN,
  ADULT
}

package wbook.wbook.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class SearchBook {
  String name;
  String categoryBook;
  LocalDate localDate;
}

package wbook.wbook.model;

public enum TypeContrat {
  FREE,
  BLOCK,
  VIP,
  BUIZ
}

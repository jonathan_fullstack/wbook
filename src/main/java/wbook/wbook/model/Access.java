package wbook.wbook.model;

public enum Access {
  READ,
  WRITE,
  ALL
}

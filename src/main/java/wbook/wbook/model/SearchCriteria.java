package wbook.wbook.model;

import lombok.Data;

@Data
public class SearchCriteria {
  public SearchCriteria(String key, Object value, SearchOperation operation) {
    this.key = key;
    this.value = value;
    this.operation = operation;
  }

  private String key;
  private Object value;
  private SearchOperation operation;

}

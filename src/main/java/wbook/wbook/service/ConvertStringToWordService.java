package wbook.wbook.service;

import org.apache.poi.xwpf.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import wbook.wbook.error.WbookExceptionSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ConvertStringToWordService {


  Logger logger = LoggerFactory.getLogger(ConvertStringToWordService.class);

  /**
   *
   * @param filename
   * @return analyse du fichier , retourne ligne par ligne
   */
  public String readDocxFile(File filename) {
    try {
      FileInputStream fis = new FileInputStream(filename.getAbsolutePath());
      XWPFDocument document = new XWPFDocument(fis);
      List<XWPFParagraph> paragraphs = document.getParagraphs();
      for (XWPFParagraph para : paragraphs) {
        return para.getText();
      }
      fis.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  public String convertTextFileToString(File fileName) throws WbookExceptionSystem {
    try (Stream<String> stream
           = Files.lines(Paths.get(ClassLoader.getSystemResource(fileName.getAbsolutePath()).toURI()))) {
      return stream.collect(Collectors.joining(" "));
    } catch (IOException | URISyntaxException e) {
      logger.error("erreur suivante: {}", e);
      throw new WbookExceptionSystem("Une erreur est survenue lors de la transformation du fichier");
    }
  }
/*
  private  void formating(NewBookDto newBookDto) throws WbookExceptionSystem {
    XWPFParagraph title = document.createParagraph();
    title.setAlignment(ParagraphAlignment.CENTER);
    XWPFRun titleRun = title.createRun();
    titleRun.setText(newBookDto.getName());
    titleRun.setColor("009933");
    titleRun.setBold(true);
    titleRun.setFontFamily("Courier");
    titleRun.setFontSize(20);

    XWPFParagraph subTitle = document.createParagraph();
    subTitle.setAlignment(ParagraphAlignment.CENTER);

    XWPFRun subTitleRun = subTitle.createRun();
    subTitleRun.setText("from HTTP fundamentals to API Mastery");
    subTitleRun.setColor("00CC44");
    subTitleRun.setFontFamily("Courier");
    subTitleRun.setFontSize(16);
    subTitleRun.setTextPosition(20);
    subTitleRun.setUnderline(UnderlinePatterns.DOT_DOT_DASH);


    XWPFParagraph para1 = document.createParagraph();
    para1.setAlignment(ParagraphAlignment.BOTH);
    String string1 = convertTextFileToString(newBookDto.getBody().stream()
      .map(n -> String.valueOf(n))
      .collect(Collectors.joining("-", "{", "}")));
    XWPFRun para1Run = para1.createRun();
    para1Run.setText(string1);
  }
*/

}

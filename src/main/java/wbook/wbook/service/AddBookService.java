package wbook.wbook.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import wbook.wbook.dto.NewBookDto;
import wbook.wbook.entity.Book;
import wbook.wbook.error.NotificationException;
import wbook.wbook.error.WbookExceptionSystem;
import wbook.wbook.mapper.BookMapper;
import wbook.wbook.repository.BookRepository;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class AddBookService {

  @Autowired
  AuthorService authorService;

  @Autowired
  BookRepository bookRepository;

  @Autowired
  NotificationException notificationException;

  @Autowired
  BookMapper bookMapper;

  Logger logger = LoggerFactory.getLogger(AddBookService.class);

  /**
   *
   * @param newBookDto
   * @return ajoute un nouveaux livre
   * @throws WbookExceptionSystem
   */
  public ResponseEntity<?> addNewBook(NewBookDto newBookDto) throws WbookExceptionSystem {
    validatorCreationBook(newBookDto);
    Book newBook;
    newBook = bookMapper.mapNewBookDtoToBook(newBookDto);
    newBook.setDate_of_create(LocalDate.now());
    newBook.setAuthor(authorService.getAuthor(newBookDto.getAuthorDto().getUserName()));
    bookRepository.save(newBook);
    return ResponseEntity.ok("Livre ajouté avec succés");
  }

  /**
   *
   * @param newBookDto
   * @return cree une version depuis un original
   * @throws WbookExceptionSystem
   */
  public ResponseEntity<?> addNewVersionBook(NewBookDto newBookDto) throws WbookExceptionSystem {
      Book versionBook = bookMapper.mapNewBookDtoToBook(newBookDto);
      List<Book> listBookByVersion = bookRepository.getListBookByName(newBookDto.getName());
      versionBook.setVersion(String.valueOf(listBookByVersion.size() + 1) );
      versionBook.setBody(newBookDto.getBody());
      versionBook.setDate_of_create(LocalDate.now());
      bookRepository.save(versionBook);
      return ResponseEntity.ok("Nouvelle version créer");
  }


  /**
   *
   * @param file
   * @return converti mutltipart file en 1 fichier
   * @throws IOException
   */
  public File convertMultiPartToFile(MultipartFile file) throws IOException {
    File convFile = new File(file.getOriginalFilename());
    FileOutputStream fos = new FileOutputStream(convFile);
    fos.write(file.getBytes());
    fos.close();
    return convFile;
  }

  /**
   *
   * @param newBookDto
   * methode pour controller l'insertion d 'un livre
   * @throws WbookExceptionSystem
   */
  private void validatorCreationBook(NewBookDto newBookDto) throws WbookExceptionSystem {
    if (!authorService.verifyExistAuthor(newBookDto.getAuthorDto().getUserName())) {
       notificationException.authorNoExistEception();
    }
    Optional<Book> book = Optional.ofNullable(bookRepository.findByName(newBookDto.getName()));
    if (book.isPresent()) {
       notificationException.bookExistException();
    }
    if (newBookDto.getResume() == null) {
       notificationException.bookResumeException();
    }
    if (newBookDto.getBody().isEmpty()) {
       notificationException.bookDataException();
    }
  }
}

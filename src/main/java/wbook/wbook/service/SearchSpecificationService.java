package wbook.wbook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import wbook.wbook.error.NotificationException;
import wbook.wbook.model.SearchCriteria;
import wbook.wbook.model.SearchOperation;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class SearchSpecificationService<T> implements Specification<T> {

  private static final long serialVersionUID = 1900581010229669687L;

  private List<SearchCriteria> list;

  @Autowired
  NotificationException notificationException;

  public SearchSpecificationService() {
    this.list = new ArrayList<>();
  }

  public void add(SearchCriteria criteria) {
    list.add(criteria);
  }

  /**
   * service de recherche
   *
   * @param root
   * @param query
   * @param builder
   * @return
   */
  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

    //create a new predicate list
    List<Predicate> predicates = new ArrayList<>();

    if (root == null) {
      notificationException.defaultSearchErrorException("root");
    }
    if (query == null) {
      notificationException.defaultSearchErrorException("query");
    }
    if (builder == null) {
      notificationException.defaultSearchErrorException("root");
    }

    //add add criteria to predicates
    for (SearchCriteria criteria : list) {
      if (criteria.getOperation().equals(SearchOperation.GREATER_THAN)) {
        predicates.add(builder.greaterThan(
          root.get(criteria.getKey()), criteria.getValue().toString()));
      } else if (criteria.getOperation().equals(SearchOperation.LESS_THAN)) {
        predicates.add(builder.lessThan(
          root.get(criteria.getKey()), LocalDate.parse(criteria.getValue().toString())));
      } else if (criteria.getOperation().equals(SearchOperation.GREATER_THAN_EQUAL)) {
        predicates.add(builder.greaterThanOrEqualTo(
          root.get(criteria.getKey()), LocalDate.parse(criteria.getValue().toString())));
      } else if (criteria.getOperation().equals(SearchOperation.LESS_THAN_EQUAL)) {
        predicates.add(builder.lessThanOrEqualTo(
          root.get(criteria.getKey()), LocalDate.parse(criteria.getValue().toString())));
      } else if (criteria.getOperation().equals(SearchOperation.NOT_EQUAL)) {
        predicates.add(builder.notEqual(
          root.get(criteria.getKey()), criteria.getValue()));
      } else if (criteria.getOperation().equals(SearchOperation.EQUAL)) {
        predicates.add(builder.equal(
          root.get(criteria.getKey()), criteria.getValue()));
      } else if (criteria.getOperation().equals(SearchOperation.LIKE)) {
        predicates.add(builder.like(
          root.get(criteria.getKey()), (String) criteria.getValue()));
      } else if (criteria.getOperation().equals(SearchOperation.MATCH)) {
        predicates.add(builder.like(
          builder.lower(root.get(criteria.getKey())),
          criteria.getValue().toString()));
      } else if (criteria.getOperation().equals(SearchOperation.MATCH_END)) {
        predicates.add(builder.like(
          builder.lower(root.get(criteria.getKey())),
          criteria.getValue().toString().toLowerCase() + "%"));
      }
    }


    return builder.and(predicates.toArray(new Predicate[0]));
  }
}

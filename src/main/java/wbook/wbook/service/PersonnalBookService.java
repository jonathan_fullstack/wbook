package wbook.wbook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import wbook.wbook.dto.AuthorDto;
import wbook.wbook.dto.BookDto;
import wbook.wbook.entity.Author;
import wbook.wbook.entity.Book;
import wbook.wbook.entity.FavoriBook;
import wbook.wbook.error.NotificationException;
import wbook.wbook.error.WbookExceptionSystem;
import wbook.wbook.mapper.BookMapper;
import wbook.wbook.repository.AuthorRepository;
import wbook.wbook.repository.BookRepository;
import wbook.wbook.repository.FavoriRepository;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class PersonnalBookService {

  @Autowired
  BookRepository bookRepository;

  @Autowired
  NotificationException notificationException;

  @Autowired
  BookMapper bookMapper;

  @Autowired
  AuthorService authorService;

  @Autowired
  AuthorRepository authorRepository;

  @Autowired
  FavoriRepository favoriRepository;

  /**
   * @param userName
   * @param id
   * @param type
   * @return ajout de livre favori et en cour de creation
   * @throws WbookExceptionSystem
   */
  public ResponseEntity<?> addFavoriOrCreateBookBook(String userName, int id, String type) throws WbookExceptionSystem {
    if (!authorService.verifyExistAuthor(userName)) {
      notificationException.authorNoExistEception();
    }

    Author author = authorService.getAuthor(userName);
    Book book = bookRepository.getBookById((long) id);
    if (book == null) {
      return notificationException.notBookException();
    }

    Optional<FavoriBook> favoriBookExist = favoriRepository.findByBook(book);
    System.out.println(favoriBookExist.get());

    if (favoriBookExist.isPresent()) {
      return notificationException.addFavoriErrorException();
    }

    FavoriBook favoriBook = new FavoriBook();
    if (type.equals("FAVORI")) {
      book.setType("FAVORI");
      favoriBook.setBook(book);
      favoriBook.setAuthor(author);
      favoriBook.setType("FAVORI");
      favoriRepository.save(favoriBook);
      return ResponseEntity.ok("Livre ajouté au favori");
    } else {
      book.setType("CREATION");
      favoriBook.setBook(book);
      favoriBook.setAuthor(author);
      favoriBook.setType("CREATION");
      favoriRepository.save(favoriBook);
      return ResponseEntity.ok("Livre en cours de création enregistré");
    }
  }

  /**
   * @param authorDto
   * @return la liste de livre favori et en cour de creation
   * @throws WbookExceptionSystem
   */
  public ResponseEntity<?> getBookByAuthor(AuthorDto authorDto) throws WbookExceptionSystem {
    if (!authorService.verifyExistAuthor(authorDto.getUserName())) {
      notificationException.authorNoExistEception();
    }
    Author author = authorService.getAuthor(authorDto.getUserName());
    List<FavoriBook> targetList = (List<FavoriBook>) favoriRepository.findAll().stream()
      .filter(b -> b.getAuthor().equals(author)).collect(Collectors.toList());

    List<Book> targetListBook = new ArrayList<>();
    targetList.forEach(
      fb -> {
        if (fb.getType().equals("FAVORI")) {
          fb.getBook().setType("FAVORI");
          targetListBook.add(fb.getBook());
        } else {
          fb.getBook().setType("CREATION");
          targetListBook.add(fb.getBook());
        }
      }
    );
    return ResponseEntity.ok(bookMapper.mapListBookToListBookDtoWithVersion(targetListBook));
  }
}

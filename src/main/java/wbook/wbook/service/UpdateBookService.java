package wbook.wbook.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import wbook.wbook.dto.BookDto;
import wbook.wbook.entity.Book;
import wbook.wbook.error.NotificationException;
import wbook.wbook.mapper.BookMapper;
import wbook.wbook.repository.BookRepository;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UpdateBookService {

  @Autowired
  BookRepository bookRepository;

  @Autowired
  NotificationException notificationException;

  @Autowired
  BookMapper bookMapper;


  Logger logger = LoggerFactory.getLogger(UpdateBookService.class);

  /**
   *
   * @param bookDto
   * @return methode pour modifier un livre en cours de creation
   */
  public ResponseEntity<?> updateBook(BookDto bookDto) {
    Optional<Book> book = Optional.ofNullable(bookRepository.getBookById((long) bookDto.getBookId()));
    if (book.isEmpty()) {
      notificationException.notBookException();
    }
    if (bookDto.getAuthorDto().getUserName() != book.get().getAuthor().getUsername()) {
      return notificationException.authorNoExistEception();
    }
    if (book.get().getBody() == null) {
      return notificationException.bookDataException();
    }
    Book updateBook = bookMapper.mapBookDtoToBook(bookDto);
    updateBook.setDate_of_update(LocalDate.now());
    bookRepository.save(updateBook);
    return ResponseEntity.ok("book update with success");
  }
}

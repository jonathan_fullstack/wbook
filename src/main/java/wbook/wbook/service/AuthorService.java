package wbook.wbook.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import wbook.wbook.dto.AuthorDto;
import wbook.wbook.dto.SigninDto;
import wbook.wbook.entity.Author;
import wbook.wbook.error.NotificationException;
import wbook.wbook.error.WbookExceptionSystem;
import wbook.wbook.mapper.AuthorMapper;
import wbook.wbook.repository.AuthorRepository;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class AuthorService {

  @Autowired
  AuthorRepository authorRepository;


  @Autowired
  AuthorMapper authorMapper;

  @Autowired
  NotificationException notificationException;

  @Autowired
  PasswordEncoder passwordEncoder;

  Logger logger = LoggerFactory.getLogger(AuthorService.class);

  /**
   * @param userName
   * @return verifyExistAuthor
   */
  public boolean verifyExistAuthor(String userName) throws WbookExceptionSystem {
    if (userName == null) {
      throw new WbookExceptionSystem("userName is null");
    }
    Optional<Author> author = Optional.ofNullable(authorRepository.getByUserName(userName));
    if (author.isEmpty()) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * @param userName
   * @return getAuthor
   */
  public Author getAuthor(String userName) throws WbookExceptionSystem {
    if (userName == null) {
      throw new WbookExceptionSystem("userName is null");
    }
    if (authorRepository.findByUsername(userName).isPresent()) {
      return authorRepository.findByUsername(userName).get();
    } else {
      throw new WbookExceptionSystem("Auteur non recuperable");
    }
  }

  /**
   *
   * @param userName
   * @return author
   * @throws WbookExceptionSystem
   */
  public AuthorDto getAuthorDto(String userName) throws WbookExceptionSystem {
    if (userName == null) {
      throw new WbookExceptionSystem("userName is null");
    }
    if (verifyExistAuthor(userName)) {
      return authorMapper.mapAuthorToAuthorDto(authorRepository.getByUserName(userName));
    } else {
      notificationException.authorNoExistEception();
      throw new WbookExceptionSystem("mappage author en echec");
    }

  }

  /**
   *
   * @param authorDto
   * @return add author in database
   * @throws WbookExceptionSystem
   */
  public ResponseEntity<?> addAuthor(AuthorDto authorDto) throws WbookExceptionSystem {
    logger.error(authorDto.getUserName(), authorDto.getFirstName());
    if (authorDto.getUserName() == null) {
      authorDto.setUserName(authorDto.getFirstName() + "-" + authorDto.getLastName());
    }
    if (authorDto.getPassword().length() < 8) {
      return notificationException.passwordLenghtException();
    }
    authorDto.setDateToCreate(LocalDate.now());
    if (verifyExistAuthor(authorDto.getUserName()) == false) {
      Author author = authorMapper.mapAuthorDtoToAuthor(authorDto);
      author.setPassword(passwordEncoder.encode(authorDto.getPassword()));
      authorRepository.save(author);
      return ResponseEntity.ok(authorDto);
    } else {
      return notificationException.authorExistException();
    }
  }

  /**
   *
   * @param signinDto
   * @return verifie les idendifiants de connexions
   */
  public ResponseEntity<?> loadAuthorByEmailAndPassword(SigninDto signinDto) {
    if (signinDto.getPassword() == null || signinDto.getEmail() == null ) {
      return notificationException.signinException();
    }
    Author author = authorRepository.findByEmail(signinDto.getEmail());
    if (author == null) {
      return notificationException.authorNoExistEception();
    } else {
      boolean isPasswordMatch = passwordEncoder.matches(signinDto.getPassword(), author.getPassword());
        if (isPasswordMatch == true) {
          return ResponseEntity.ok(authorMapper.mapAuthorToAuthorDto(author));
        } else {
          return notificationException.passwordErrorException();
        }
    }
  }
}

package wbook.wbook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import wbook.wbook.dto.AuthorDto;
import wbook.wbook.dto.BookDto;
import wbook.wbook.dto.VersionBookDto;
import wbook.wbook.entity.Book;
import wbook.wbook.entity.BookUpdate;
import wbook.wbook.error.NotificationException;
import wbook.wbook.mapper.BookMapper;
import wbook.wbook.mapper.BookUpdateMapper;
import wbook.wbook.model.CategoryBook;
import wbook.wbook.model.SearchCriteria;
import wbook.wbook.model.SearchOperation;
import wbook.wbook.repository.BookRepository;
import wbook.wbook.repository.BookUpdateRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import java.util.stream.Collectors;

@Service
public class GetBookService {

  @Autowired
  BookRepository bookRepository;

  @Autowired
  BookMapper bookMapper;

  @Autowired
  NotificationException notificationException;

  @Autowired
  BookUpdateRepository bookUpdateRepository;

  @Autowired
  BookUpdateMapper bookUpdateMapper;

  @Autowired
  SearchSpecificationService searchSpecificationService;

  @Autowired
  EntityManager em;

  /**
   * @param authorDto
   * @return getAllBookWithVersion
   */
  public ResponseEntity<?> getAllBookWithVersion(AuthorDto authorDto) {
    List<Book> allBook = bookRepository.findAll();
    if (allBook.isEmpty()) {
      return notificationException.notBookException();
    }
    return ResponseEntity.ok(bookMapper.mapListBookToListBookDtoWithVersion(allBook));
  }

  /**
   * @return getAllBookWithoutVersionWithLimitto10
   */
  public ResponseEntity<?> getAllBookWithoutVersionWithView10Book() {
    LocalDate localDateNow = LocalDate.now();
    LocalDate localDateNowMinius7 = LocalDate.now().minusDays(7);
    LocalDate localDateNowMinius14 = LocalDate.now().minusDays(14);
    LocalDate localDateNowMinius30 = LocalDate.now().minusDays(30);
    List<Book> allBook = bookRepository.getListBookWithParams7(localDateNow, localDateNowMinius7);
    if (allBook.size() < 1) {
      allBook = bookRepository.getListBookWithParams14(localDateNow, localDateNowMinius14);
    } else if (allBook.size() < 2) {
      allBook = bookRepository.getListBookWithParams30(localDateNow, localDateNowMinius30);
    }

    if (allBook.isEmpty()) {
      allBook = bookRepository.findAll();
    } else  {
      return notificationException.notBookException();
    }

    return ResponseEntity.ok(bookMapper.mapListBookToListBookDtoWithoutVersion(allBook).stream()
      .limit(10)
      .collect(Collectors.toList()));
  }

  /**
   * @return getAllBookWithoutVersion
   */
  public ResponseEntity<?> getAllBookWithoutVersion() {
    List<Book> allBook = bookRepository.findAll();
    if (allBook.isEmpty()) {
      return notificationException.notBookException();
    }
    return ResponseEntity.ok(bookMapper.mapListBookToListBookDtoWithoutVersion(allBook));
  }



  /**
   * @param name
   * @param categoryBook
   * @Param localDateOfCreate
   * @return getBookDtoWithSearchMultiCriter
   */
  public ResponseEntity<?> getBookDtoWithSearchMultiCriter(String name, String categoryBook, LocalDate date_of_create) {
   LocalDate localDate = LocalDate.now();
    List<Book> allBook = new ArrayList<>();
    List<Book> allBookFilterName = new ArrayList<>();
    List<Book> allBookFilterCategoryBook = new ArrayList<>();
   if (name != null) {
     allBookFilterName = bookRepository.getListByMultiCriterWithName(name);
   }
     if (categoryBook != null) {
       allBookFilterCategoryBook = bookRepository.getListByMultiCriterWithCategoryBook(categoryBook);
     }
    System.out.println("ooooooooooooooooooooooooooooooo" + allBookFilterName.size());
    System.out.println("ooooooooooooooooooooooooooooooo" + allBookFilterCategoryBook.size());
   allBook.addAll(allBookFilterName);
   allBook.addAll(allBookFilterCategoryBook);


    if (allBook.isEmpty()) {
      return notificationException.notBookException();
    } else {

      return ResponseEntity.ok(bookMapper.mapListBookToListBookDtoWithoutVersion(allBook).stream()
          .filter(b -> localDate.isAfter(date_of_create))
        .limit(60)
        .collect(Collectors.toList()));
    }
  }

  public ResponseEntity<?> getBookById(int id) {
    Book book = bookRepository.getBookById((long) id);
    if (book == null) {
      return notificationException.notBookException();
    } else {
      return ResponseEntity.ok(bookMapper.mapBookToBookDto(book));
    }
  }

  public ResponseEntity<?> getListVersionBookDtoByBook(int bookId) {
    return ResponseEntity.ok("method a cree");
  }
}

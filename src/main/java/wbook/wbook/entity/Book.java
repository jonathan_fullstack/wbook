package wbook.wbook.entity;

import lombok.Data;

import wbook.wbook.model.CategoryBook;
import wbook.wbook.model.Provider;
import wbook.wbook.model.TypeContrat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@Entity
@Table(name = "t_book")
public class Book {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  long book_id;
  String name;
  String version;
  @Column(name = "body", columnDefinition = "longtext")
  String body;
  String resume;
  LocalDate date_of_create;
  LocalDate date_of_update;
  boolean block;
  String category_book;
  Double value;
  TypeContrat type_contrat;
  int notes;
  boolean accesible;
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "author_id", nullable = false)
  Author author;
  long optionAttachment;
  @OneToMany(mappedBy = "optionAttachment")
  Set<Book> book_version;

  String type;

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }


  public long getBook_id() {
    return book_id;
  }

  public void setBook_id(long book_id) {
    this.book_id = book_id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }


  public String getResume() {
    return resume;
  }

  public void setResume(String resume) {
    this.resume = resume;
  }

  public LocalDate getDate_of_create() {
    return date_of_create;
  }

  public void setDate_of_create(LocalDate date_of_create) {
    this.date_of_create = date_of_create;
  }

  public LocalDate getDate_of_update() {
    return date_of_update;
  }

  public void setDate_of_update(LocalDate date_of_update) {
    this.date_of_update = date_of_update;
  }


  public boolean isBlock() {
    return block;
  }

  public void setBlock(boolean block) {
    this.block = block;
  }

  public String getCategory_book() {
    return category_book;
  }

  public void setCategory_book(String category_book) {
    this.category_book = category_book;
  }

  public Double getValue() {
    return value;
  }

  public void setValue(Double value) {
    this.value = value;
  }

  public TypeContrat getType_contrat() {
    return type_contrat;
  }

  public void setType_contrat(TypeContrat type_contrat) {
    this.type_contrat = type_contrat;
  }

  public int getNotes() {
    return notes;
  }

  public void setNotes(int notes) {
    this.notes = notes;
  }

  public boolean isAccesible() {
    return accesible;
  }

  public void setAccesible(boolean accesible) {
    this.accesible = accesible;
  }

  public Author getAuthor() {
    return author;
  }

  public void setAuthor(Author author) {
    this.author = author;
  }

}

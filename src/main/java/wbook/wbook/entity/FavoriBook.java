package wbook.wbook.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "t_favori_book")
@Data
public class FavoriBook {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  long favori_book_id;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = "author_id")
  Author author;

  @OneToOne
  @JoinColumn(name = "book_book_id")
  Book book;

  String type;

}

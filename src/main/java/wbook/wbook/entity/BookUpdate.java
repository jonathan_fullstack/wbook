package wbook.wbook.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "t_book_update")
public class BookUpdate {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  long book_update_id;
  @OneToOne
  @JoinColumn(name = "authorId")
  Author author;
  String body;
  LocalDate date_of_update;
  LocalDate date_of_create;
  boolean block;
  int notes;
  String version;
  String resume;
  @ManyToOne
  @JoinColumn(name="book_id", nullable=false)
  Book book;

  public long getBook_update_id() {
    return book_update_id;
  }

  public void setBook_update_id(long book_update_id) {
    this.book_update_id = book_update_id;
  }

  public Author getAuthor() {
    return author;
  }

  public void setAuthor(Author author) {
    this.author = author;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public LocalDate getDate_of_update() {
    return date_of_update;
  }

  public void setDate_of_update(LocalDate date_of_update) {
    this.date_of_update = date_of_update;
  }

  public LocalDate getDate_of_create() {
    return date_of_create;
  }

  public void setDate_of_create(LocalDate date_of_create) {
    this.date_of_create = date_of_create;
  }

  public boolean isBlock() {
    return block;
  }

  public void setBlock(boolean block) {
    this.block = block;
  }

  public int getNotes() {
    return notes;
  }

  public void setNotes(int notes) {
    this.notes = notes;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getResume() {
    return resume;
  }

  public void setResume(String resume) {
    this.resume = resume;
  }

  public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  String name;




}

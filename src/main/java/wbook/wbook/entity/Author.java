package wbook.wbook.entity;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import wbook.wbook.model.Provider;
import wbook.wbook.model.SocialStatus;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@Entity
@DynamicUpdate
@Table(name = "t_author")
public class Author {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  long author_id;
  @OneToMany(mappedBy="book_id")
  Set<Book> book;
  String username;
  String lastname;
  String firstname;
  String password;
  String email;
  @Enumerated(EnumType.STRING)
  private Provider provider;
  @Enumerated(EnumType.STRING)
  SocialStatus social_status;
  @OneToMany(mappedBy="book_update_id")
  Set<BookUpdate> book_update;
  LocalDate date_to_create;

  @OneToMany(mappedBy = "book_id")
  Set<Book> favori_book;

  @OneToMany(mappedBy = "book_id")
  Set<Book> creatBook;

}

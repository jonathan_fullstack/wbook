package wbook.wbook.entity;

import wbook.wbook.model.CategoryBook;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;

public class BookManager {
  EntityManager em;


  List<Book> findBooksByNameAndCategoryBookAndDateOfCreate(String name, String categoryBook, LocalDate date_of_create) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<Book> cq = cb.createQuery(Book.class);

    Root<Book> book = cq.from(Book.class);
    Predicate namePredicate = cb.like(book.get("name"), name);
    Predicate categroyBookPredicate = cb.like(book.get("categoryBook"), "%" + categoryBook + "%");
    Predicate dateOfCreatePredicate = cb.lessThanOrEqualTo(book.get("date_of_create"), date_of_create);
    cq.where(namePredicate, categroyBookPredicate, dateOfCreatePredicate);

    TypedQuery<Book> query = em.createQuery(cq);
    return query.getResultList();
  }
}

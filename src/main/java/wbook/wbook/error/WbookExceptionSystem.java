package wbook.wbook.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class WbookExceptionSystem extends  Exception{
  public WbookExceptionSystem(String message) {
    super(message);
  }
}

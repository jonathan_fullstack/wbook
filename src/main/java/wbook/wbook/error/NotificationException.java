package wbook.wbook.error;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

@Service
public class NotificationException  {

  public ResponseEntity<String> authorNoExistEception() {
    return ResponseEntity.status(505).body("Author dont exist");
  }

  public ResponseEntity<String> bookNoTitleEception() {
    return ResponseEntity.status(505).body("title in this book");
  }

  public ResponseEntity<String> bookDataException() {
    return ResponseEntity.status(505).body("No data in this book");
  }

  public ResponseEntity<String> bookResumeException() {
    return ResponseEntity.status(505).body("No resume in this book");
  }

  public ResponseEntity<String> bookExistException() {
    return ResponseEntity.status(505).body("this book with title already exists");
  }

  public ResponseEntity<String> notRuleException() {
    return ResponseEntity.status(505).body("not right for update this book");
  }

  public ResponseEntity<String> notBookException() {
    return ResponseEntity.status(506).body("not book for this moment");
  }

  public ResponseEntity<String> notBookSearchException() {
    return ResponseEntity.status(506).body("Pas de livre avec ses arguments");
  }

  public ResponseEntity<String> authorExistException() {
    return ResponseEntity.status(506).body("Cette autheur existe déja");
  }

  public ResponseEntity<String> ConvertFileToStringException() {
    return ResponseEntity.status(506).body("Une erreur est survenue lors de la transformation du fichier");
  }

  public ResponseEntity<String> passwordLenghtException() {
    return ResponseEntity.status(506).body("Votre mot de passe n 'est pas conforme !");
  }

  public ResponseEntity<String> signinException() {
    return ResponseEntity.status(506).body("Il manque le mot de passe ou l'email");
  }

  public ResponseEntity<String> passwordErrorException() {
    return ResponseEntity.status(506).body("Le mot de passe ne correspond pas");
  }

  public ResponseEntity<String> defaultSearchErrorException(String search) {
    return ResponseEntity.status(509).body("erreur avec le criteres suivant" + search);
  }

  public ResponseEntity<String> addFavoriErrorException() {
    return ResponseEntity.status(506).body("Ce livre est déja dans vos favori");
  }

}

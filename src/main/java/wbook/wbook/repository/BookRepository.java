package wbook.wbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import wbook.wbook.entity.Book;
import wbook.wbook.model.CategoryBook;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {
  boolean existsByName(String name);

  @Query(value = "SELECT * FROM t_book b WHERE b.book_id = :bookId",
    nativeQuery = true)
  Book getBookById(@Param("bookId") Long bookId);

  Book findByName(String title);

  @Query(value = "SELECT * FROM t_book b WHERE b.name = :name", nativeQuery = true)
  List<Book> getListBookByName(@Param("name") String name);


  @Query(value = "SELECT * FROM t_book b WHERE b.date_of_create >= :localDateNow AND b.date_of_create <= :localDateMinius7", nativeQuery = true)
  List<Book> getListBookWithParams7(@Param(("localDateNow")) LocalDate localDateNow,@Param("localDateMinius7") LocalDate localDateMinius7 );


  @Query(value = "SELECT * FROM t_book b WHERE b.date_of_create >= :localDateNow AND b.date_of_create <= :localDateMinius14", nativeQuery = true)
  List<Book> getListBookWithParams14(@Param(("localDateNow")) LocalDate localDateNow,@Param("localDateMinius14") LocalDate localDateMinius14 );


  @Query(value = "SELECT * FROM t_book b WHERE b.date_of_create >= :localDateNow AND b.date_of_create <= :localDateMinius30", nativeQuery = true)
  List<Book> getListBookWithParams30(@Param(("localDateNow")) LocalDate localDateNow,@Param("localDateMinius30") LocalDate localDateMinius30 );

  @Query("SELECT b  from Book b  WHERE   b.name LIKE :name")
  List<Book> getListByMultiCriterWithName(@Param("name") String name);

  @Query("SELECT b  from Book b  WHERE  b.category_book = :categoryBook")
  List<Book> getListByMultiCriterWithCategoryBook(@Param("categoryBook")String categoryBook);
}

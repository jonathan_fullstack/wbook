package wbook.wbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import wbook.wbook.entity.Author;

import java.util.Optional;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
  Optional<Author> findByUsername(String userName);

  @Query("SELECT a FROM Author a WHERE a.username = :username")
  Author getByUserName(@Param("username") String username);

  Author findByEmail(String email);
}

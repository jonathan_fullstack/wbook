package wbook.wbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import wbook.wbook.entity.Book;
import wbook.wbook.entity.FavoriBook;

import java.util.Optional;

public interface FavoriRepository extends JpaRepository<FavoriBook, Long> {
  Optional<FavoriBook> findByBook(Book book);
}

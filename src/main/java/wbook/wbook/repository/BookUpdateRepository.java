package wbook.wbook.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import wbook.wbook.entity.BookUpdate;

@Repository
public interface BookUpdateRepository extends JpaRepository<BookUpdate, Long> {
  boolean findByName(String name);
}

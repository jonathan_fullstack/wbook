package wbook.wbook.dto;

import lombok.Data;
import wbook.wbook.model.CategoryBook;
import wbook.wbook.model.SocialStatus;

import java.time.LocalDate;
import java.util.List;

@Data
public class VersionBookDto {
  int bookId;

  public int getBookId() {
    return bookId;
  }

  public void setBookId(int bookId) {
    this.bookId = bookId;
  }

  public int getBookVersionId() {
    return bookVersionId;
  }

  public void setBookVersionId(int bookVersionId) {
    this.bookVersionId = bookVersionId;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getResume() {
    return resume;
  }

  public void setResume(String resume) {
    this.resume = resume;
  }

  public LocalDate getDateOfCreate() {
    return dateOfCreate;
  }

  public void setDateOfCreate(LocalDate dateOfCreate) {
    this.dateOfCreate = dateOfCreate;
  }

  public AuthorDto getAuthorDto() {
    return authorDto;
  }

  public void setAuthorDto(AuthorDto authorDto) {
    this.authorDto = authorDto;
  }

  public SocialStatus getSocialStatus() {
    return socialStatus;
  }

  public void setSocialStatus(SocialStatus socialStatus) {
    this.socialStatus = socialStatus;
  }

  public String getCategoryBook() {
    return categoryBook;
  }

  public void setCategoryBook(String categoryBook) {
    this.categoryBook = categoryBook;
  }

  public boolean isAccesible() {
    return accesible;
  }

  public void setAccesible(boolean accesible) {
    this.accesible = accesible;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  int bookVersionId;
  String body;
  String resume;
  LocalDate dateOfCreate;
  AuthorDto authorDto;
  SocialStatus socialStatus;
  String categoryBook;
  boolean accesible;
  String version;
  String name;
}

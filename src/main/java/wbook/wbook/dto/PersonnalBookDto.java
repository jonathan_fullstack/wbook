package wbook.wbook.dto;

import lombok.Data;

import java.util.List;

@Data
public class PersonnalBookDto {
  int bookFavoriId;
  List<BookDto> bookDto;
}

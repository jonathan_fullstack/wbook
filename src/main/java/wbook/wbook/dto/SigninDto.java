package wbook.wbook.dto;

import lombok.Data;

@Data
public class SigninDto {
  String email;
  String password;
}

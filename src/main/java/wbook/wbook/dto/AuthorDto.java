package wbook.wbook.dto;

import lombok.Data;
import wbook.wbook.model.Provider;
import wbook.wbook.model.SocialStatus;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Data
public class AuthorDto {
  String userName;
  String firstName;
  String lastName;
  String password;
  String email;
  LocalDate dateToCreate;
  @Enumerated(EnumType.STRING)
  private Provider provider;
  @Enumerated(EnumType.STRING)
  SocialStatus social_status;
}

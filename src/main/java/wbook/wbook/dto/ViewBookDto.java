package wbook.wbook.dto;

import lombok.Data;
import wbook.wbook.model.CategoryBook;

import java.time.LocalDate;

@Data
public class ViewBookDto {
  LocalDate dateOfCreate;
  String resume;
  String name;
  AuthorDto authorDto;
  int id;
  String categoryBook;
  String body;

}

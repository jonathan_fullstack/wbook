package wbook.wbook.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class NewVersionBookDto {
  private  String resume;
  private  LocalDate dateOfCreate;
  private String body;
  private int originalBook;
  private  AuthorDto authorDto;
}

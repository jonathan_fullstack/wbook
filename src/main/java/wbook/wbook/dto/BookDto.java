package wbook.wbook.dto;

import lombok.Data;
import wbook.wbook.model.CategoryBook;

import java.time.LocalDate;
import java.util.List;

@Data
public class BookDto {
  int bookId;
  String body;
  String resume;
  String name;
  LocalDate dateOfCreate;
  String categoryBook;
  AuthorDto authorDto;
  String version;
  List<VersionBookDto> listVersionBookDto;
  String type;

}

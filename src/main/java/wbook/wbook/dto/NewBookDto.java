package wbook.wbook.dto;

import lombok.Data;
import wbook.wbook.model.CategoryBook;
import wbook.wbook.model.SocialStatus;

import java.time.LocalDate;
import java.util.List;

@Data
public class NewBookDto {

  String name;
  String body;
  String resume;
  LocalDate dateOfCreate;
  AuthorDto authorDto;
  SocialStatus socialStatus;
  String categoryBook;
  String version;
  boolean accesible;
}

package wbook.wbook;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.Query;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import wbook.wbook.repository.AuthorRepository;
import wbook.wbook.repository.BookRepository;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.io.File;

@SpringBootApplication()

public class WbookApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(WbookApplication.class, args);

	}

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
    return builder.sources(WbookApplication.class);
  }

  @Bean
  public CommandLineRunner start(BookRepository bookRepository,
                                 AuthorRepository authorRepository,
                                 DataSource dataSource) {
    return args -> {
      if(org.springframework.util.CollectionUtils.isEmpty(authorRepository.findAll())) {
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator(false, false, "UTF-8", new ClassPathResource("authorSql.sql"));
        resourceDatabasePopulator.execute(dataSource);
      }

      if(org.springframework.util.CollectionUtils.isEmpty(bookRepository.findAll())) {
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator(false, false, "UTF-8", new ClassPathResource("bookSql.sql"));
        resourceDatabasePopulator.execute(dataSource);
      }
    };
  }

}

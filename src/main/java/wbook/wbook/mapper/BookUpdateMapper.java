package wbook.wbook.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import wbook.wbook.dto.NewVersionBookDto;
import wbook.wbook.dto.VersionBookDto;
import wbook.wbook.entity.BookUpdate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class BookUpdateMapper {

  @Autowired
  AuthorMapper authorMapper;

  public BookUpdate mapBookUpdateDtoToBookUpdate(NewVersionBookDto newVersionBookDto) {
    BookUpdate bookUpdate = new BookUpdate();
    bookUpdate.setAuthor(authorMapper.mapAuthorDtoToAuthor(newVersionBookDto.getAuthorDto()));
    return  bookUpdate;
  }

  public List<VersionBookDto> mapListBookUpdateToListVersionBookDto(List<BookUpdate> bookUpdateList) {
    VersionBookDto versionBookDto = new VersionBookDto();
    List<VersionBookDto> bookDtoList = new ArrayList<>();
    for(BookUpdate bookUpdate: bookUpdateList){
      versionBookDto.setVersion(bookUpdate.getVersion());
      versionBookDto.setBody(bookUpdate.getBody());
      versionBookDto.setResume(bookUpdate.getResume());
    }
    return bookDtoList;
  }
}

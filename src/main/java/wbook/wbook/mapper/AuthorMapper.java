package wbook.wbook.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import wbook.wbook.dto.AuthorDto;
import wbook.wbook.entity.Author;

@Component
public class AuthorMapper {

  @Autowired
  PasswordEncoder passwordEncoder;

  public Author mapAuthorDtoToAuthor(AuthorDto authorDto) {
    Author author = new Author();
    author.setFirstname(authorDto.getFirstName());
    author.setLastname(authorDto.getLastName());
    author.setUsername(authorDto.getUserName());
    author.setEmail(authorDto.getEmail());
    author.setSocial_status(authorDto.getSocial_status());

    return author;
  }

  public AuthorDto mapAuthorToAuthorDto(Author author) {
    AuthorDto authorDto = new AuthorDto();
    authorDto.setUserName(author.getUsername());
    authorDto.setFirstName(author.getFirstname());
    authorDto.setLastName(author.getLastname());
    authorDto.setEmail(author.getEmail());
    authorDto.setSocial_status(author.getSocial_status());
    return authorDto;
  }
}

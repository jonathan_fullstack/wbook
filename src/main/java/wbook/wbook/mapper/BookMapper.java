package wbook.wbook.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import wbook.wbook.dto.BookDto;
import wbook.wbook.dto.NewBookDto;
import wbook.wbook.dto.ViewBookDto;
import wbook.wbook.entity.Book;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Component
public class BookMapper {

  @Autowired
  AuthorMapper authorMapper;

  /**
   *
   * @param bookDto
   * @return map book dto en book
   */
  public Book mapBookDtoToBook(BookDto bookDto) {
    Book book = new Book();
    book.setCategory_book(bookDto.getCategoryBook());
    book.setResume(bookDto.getResume());
    book.setAuthor(authorMapper.mapAuthorDtoToAuthor(bookDto.getAuthorDto()));
    return book;
  }

  /**
   *
   * @param newBookDto
   * @return map newbook ddo en book
   */
  public Book mapNewBookDtoToBook(NewBookDto newBookDto) {
    Book book = new Book();
    book.setName(newBookDto.getName());
    book.setCategory_book(newBookDto.getCategoryBook());
    book.setResume(newBookDto.getResume());
    book.setBody(newBookDto.getBody());
    book.setVersion(newBookDto.getVersion());
    book.setAuthor(authorMapper.mapAuthorDtoToAuthor(newBookDto.getAuthorDto()));
    return book;
  }

  /**
   *
   * @param book
   * @return map book en book dto
   */
  public BookDto mapBookToBookDto(Book book) {
    BookDto bookDto = new BookDto();
    bookDto.setName(book.getName());
    bookDto.setCategoryBook(book.getCategory_book());
    bookDto.setResume(book.getResume());
    bookDto.setAuthorDto(authorMapper.mapAuthorToAuthorDto(book.getAuthor()));
    bookDto.setBody(book.getBody());
    return bookDto;
  }

  /**
   *
   * @param bookList
   * @return map list book en list book dto
   */
  public Collection<BookDto> mapListBookToListBookDtoWithVersion(List<Book> bookList) {
    List<BookDto> bookDtoList = new ArrayList<>();
    BookDto bookDto = new BookDto();
    for (Book book : bookList) {
      bookDto.setBookId((int) book.getBook_id());
      bookDto.setCategoryBook(book.getCategory_book());
      bookDto.setBody(book.getBody());
      bookDto.setVersion(book.getVersion());
      bookDto.setType(book.getType());
      bookDto.setAuthorDto(authorMapper.mapAuthorToAuthorDto(book.getAuthor()));
      bookDtoList.add(bookDto);

    }
    return bookDtoList;
  }

  /**
   *
   * @param book
   * @return map new book en book dto
   */
  public ViewBookDto mapNewBookToViewBookDto(Book book) {
    ViewBookDto newViewBookDto = new ViewBookDto();
    newViewBookDto.setDateOfCreate(book.getDate_of_create());
    newViewBookDto.setName(book.getName());
    newViewBookDto.setResume(book.getResume());
    newViewBookDto.setId((int) book.getBook_id());
    newViewBookDto.setCategoryBook(book.getCategory_book());
    newViewBookDto.setAuthorDto(authorMapper.mapAuthorToAuthorDto(book.getAuthor()));
    return newViewBookDto;
  }

  /**
   *
   * @param bookList
   * @return map list book en book dto pour la creation de body sans fichier
   */
  public List<ViewBookDto> mapListBookToListBookDtoWithoutVersion(List<Book> bookList) {
    List<ViewBookDto> bookDtoList = new ArrayList<>();
    for (Book book : bookList) {
      if (book.getBody() != null) {
        bookDtoList.add(mapNewBookToViewBookDto(book));
      }

    }
    return bookDtoList;
  }
}

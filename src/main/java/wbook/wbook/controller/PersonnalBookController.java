package wbook.wbook.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wbook.wbook.dto.AuthorDto;
import wbook.wbook.error.WbookExceptionSystem;
import wbook.wbook.service.PersonnalBookService;
import wbook.wbook.utils.WebConstant;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(WebConstant.REQUESTURL + "/getPersonnalBook")
public class PersonnalBookController {

  Logger logger = LoggerFactory.getLogger(PersonnalBookController.class);

  @Autowired
  PersonnalBookService personnalBookService;

  /**
   *
   * @param userName
   * @param id
   * @param type
   * @return ajout d un livre favori ou en cour de creation
   * @throws WbookExceptionSystem
   */
  @GetMapping(value = "/addPersonnalBook")
  public ResponseEntity<?> addPersonnalBook(@RequestParam(value="userName", required = false) String userName, @RequestParam(value="id", required = false) String id, @RequestParam(value="type", required = false) String type) throws WbookExceptionSystem {
    return personnalBookService.addFavoriOrCreateBookBook(userName, Integer.valueOf(id), type);
  }

  /**
   *
   * @param authorDto
   * @return la liste des livres favoris et en cour de création
   * @throws WbookExceptionSystem
   */
  @PostMapping(value = "/getBook")
  public ResponseEntity<?> getPersonnalBook(@RequestBody AuthorDto authorDto) throws WbookExceptionSystem {
    System.out.println("reeeeeeeeeeeeeeeeeeee" + authorDto);
    return personnalBookService.getBookByAuthor(authorDto);
  }
}

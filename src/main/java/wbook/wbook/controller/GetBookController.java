package wbook.wbook.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wbook.wbook.model.CategoryBook;
import wbook.wbook.service.GetBookService;
import wbook.wbook.utils.WebConstant;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(WebConstant.REQUESTURL + "/getBook")
public class GetBookController {

  Logger logger = LoggerFactory.getLogger(GetBookController.class);

  @Autowired
  GetBookService getBookService;

  @GetMapping(value = "/listBook-10")
  public ResponseEntity<?> getAllBookWithoutVersion() {
  return getBookService.getAllBookWithoutVersionWithView10Book();
  }

  /**
   * liste complêtes de livres
   * @param bookId
   * @return
   */
  @GetMapping(value = "getAllVersionBookByBook")
  public ResponseEntity<?> getListVersionBookDtoByBook(@RequestPart int bookId) {
  return getBookService.getListVersionBookDtoByBook(bookId);
  }

  /**
   * recherche de livre par id
   * @param id
   * @return
   */
  @GetMapping(value = "/getBookById")
  public ResponseEntity<?> getBokkById(@RequestParam("id") String id) {
    return getBookService.getBookById(Integer.parseInt(id));
  }

  /**
   * Recherche multi-critere
   * @param name
   * @param categoryBook
   * @param dateOfCreate
   * @return
   */
  @CrossOrigin(origins = "*")
  @GetMapping(value = "/multicriter")
  public  ResponseEntity<?> getListBookByMulticriter(@RequestParam(value="name", required = false) String name, @RequestParam(value="categoryBook", required = false) String categoryBook, @RequestParam(value="dateOfCreate", required = false) String dateOfCreate) {
    return getBookService.getBookDtoWithSearchMultiCriter(name, categoryBook, LocalDate.parse(dateOfCreate));
  }
}

package wbook.wbook.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import wbook.wbook.dto.AuthorDto;
import wbook.wbook.dto.NewBookDto;
import wbook.wbook.error.WbookExceptionSystem;
import wbook.wbook.service.AddBookService;
import wbook.wbook.service.AuthorService;
import wbook.wbook.service.ConvertStringToWordService;
import wbook.wbook.utils.WebConstant;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(WebConstant.REQUESTURL + "/addBook")
public class AddBookController {

  @Autowired
  AddBookService addBookService;

  @Autowired
  ConvertStringToWordService convertStringToWordService;

  @Autowired
  AuthorService authorService;

  Logger logger = LoggerFactory.getLogger(AddBookController.class);

  @PostMapping(value = "/withFile")
  public ResponseEntity<?> addBookWithFile(@RequestParam("body") MultipartFile multipartFile,
                                           @RequestParam("name") String name,
                                           @RequestParam("resume") String resume,
                                           @RequestParam("authorDto") String authorDto,
                                           @RequestParam("categoryBook") String categoryBook

  ) throws WbookExceptionSystem, IOException {
    NewBookDto newBookDto = new NewBookDto();
    newBookDto.setCategoryBook(categoryBook);
    newBookDto.setResume(resume);
    newBookDto.setName(name);
    if (authorService.getAuthorDto(authorDto) == null) {
      return ResponseEntity.status(506).body("Aucun auteur trouvé");
    }
    AuthorDto authorDto1 = authorService.getAuthorDto(authorDto);
    if (authorDto1 != null) {
      newBookDto.setAuthorDto(authorDto1);
    }

    List<String> listBody = new ArrayList<>();
    File file = addBookService.convertMultiPartToFile(multipartFile);
    listBody.add(convertStringToWordService.readDocxFile(file));
    newBookDto.setBody(convertStringToWordService.readDocxFile(file));
    return addBookService.addNewBook(newBookDto);
  }

  @PostMapping(value = "/noFile")
  public ResponseEntity<?> addBookNoFile(@RequestParam("body") String body,
                                         @RequestParam("name") String name,
                                         @RequestParam("resume") String resume,
                                         @RequestParam("authorDto") String authorDto,
                                         @RequestParam("categoryBook") String categoryBook

  ) throws WbookExceptionSystem, IOException {
    NewBookDto newBookDto = new NewBookDto();
    newBookDto.setCategoryBook(categoryBook);
    newBookDto.setResume(resume);
    newBookDto.setName(name);
    if (authorService.getAuthorDto(authorDto) == null) {
      return ResponseEntity.status(506).body("Aucun auteur trouvé");
    }
    AuthorDto authorDto1 = authorService.getAuthorDto(authorDto);
    if (authorDto1 != null) {
      newBookDto.setAuthorDto(authorDto1);
    }
    newBookDto.setBody(body);
    return addBookService.addNewBook(newBookDto);
  }

  @PostMapping(value = "/newVersion")
  public ResponseEntity<?> addNewVersion(@RequestParam("body") String body,
                                         @RequestParam("name") String name,
                                         @RequestParam("resume") String resume,
                                         @RequestParam("authorDto") String authorDto,
                                         @RequestParam("categoryBook") String categoryBook

  ) throws WbookExceptionSystem, IOException {
    NewBookDto newBookDto = new NewBookDto();
    newBookDto.setCategoryBook(categoryBook);
    newBookDto.setResume(resume);
    newBookDto.setName(name);
    if (authorService.getAuthorDto(authorDto) == null) {
      return ResponseEntity.status(506).body("Aucun auteur trouvé");
    }
    AuthorDto authorDto1 = authorService.getAuthorDto(authorDto);
    if (authorDto1 != null) {
      newBookDto.setAuthorDto(authorDto1);
    }
    newBookDto.setBody(body);
    return addBookService.addNewVersionBook(newBookDto);
  }

}

package wbook.wbook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wbook.wbook.dto.AuthorDto;
import wbook.wbook.dto.SigninDto;
import wbook.wbook.error.WbookExceptionSystem;
import wbook.wbook.service.AuthorService;
import wbook.wbook.utils.WebConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(WebConstant.REQUESTURL + "/author")
public class AuthorController {

  @Autowired
  AuthorService authorService;

  @PostMapping(value = "/addAuthor")
  public ResponseEntity<?> addAuthor(@RequestBody AuthorDto authorDto) throws WbookExceptionSystem {
  return  authorService.addAuthor(authorDto);
  }

  @PostMapping(value = "/signin")
  public ResponseEntity<?> signin(@RequestBody SigninDto signinDto) {
    return authorService.loadAuthorByEmailAndPassword(signinDto);
  }

}
